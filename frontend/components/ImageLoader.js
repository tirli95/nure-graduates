// @flow
import React, { Component } from 'react';

import t from '../helpers/locale';
import { LIGHT_BLUE_ACCENT_COLOR } from '../constants';

const isClient = typeof window !== 'undefined';
const AvatarCropper = isClient ? require('react-avatar-cropper') : null;

class ImageLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      croppedImg: `${process.env.APIHOST}${props.value}`,
      cropperOpen: false,
    };
  }

  props: {
    value: string,
    onSelect: Function,
  }

  handleFile = (e) => {
    const reader = new FileReader();
    const file = e.target.files[0];

    if (!file) return;

    reader.onload = (img) => {
      this.input.value = '';
      this.setState({
        img: img.target.result,
        cropperOpen: true,
        croppedImg: this.state.croppedImg,
      });
    };

    reader.readAsDataURL(file);
  }
  handleCrop = (dataURI) => {
    this.setState({
      img: null,
      cropperOpen: false,
      croppedImg: dataURI,
    });
    this.props.onSelect(dataURI);
  }
  handleRequestHide = () => {
    this.setState({
      cropperOpen: false,
    });
  }

  render() {
    return (
      <div>
        <div className="avatar-photo">
          <input ref={(e) => { this.input = e; }} type="file" accept="image/*" onChange={this.handleFile} />
          <div className="avatar-edit">
            <i className="photo-icon" />
            <span>{t('pickPhoto')}</span>
          </div>
          {this.state.croppedImg && <img src={this.state.croppedImg} alt="" />}
        </div>
        {this.state.cropperOpen &&
          <AvatarCropper
            onRequestHide={this.handleRequestHide}
            cropperOpen={this.state.cropperOpen}
            onCrop={this.handleCrop}
            image={this.state.img}
            width={200}
            height={200}
          />
        }

        <style jsx>{`
          .avatar-photo {
            position: relative;
            background-color: white;
            height: 200px;
            width: 200px;
            overflow: hidden;
            border: 1px solid ${LIGHT_BLUE_ACCENT_COLOR};
            z-index: 3;
          }

          .avatar-photo input[type=file] {
            position: absolute;
            top:0;
            bottom: 0;
            left: 0;
            right: 0;
            width: 100%;
            height: 100%;
            cursor: pointer;
            opacity: 0;
            z-index: 9;
          }

          .avatar-edit {
            display: flex;
            position: absolute;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            height: 100%;
          }

          .avatar-edit span {
            transition: all 0.3s ease;
            text-align: center;
            z-index: 5;
            color: ${LIGHT_BLUE_ACCENT_COLOR};
          }

          .photo-icon {
            z-index: 5;
            opacity: 0.2;
            width: 100px;
            height: 100px;
            background-image: url(../static/images/camera.svg);
            background-repeat: no-repeat;
            transition: all 0.3s ease;
          }
          .avatar-photo:hover .photo-icon {
            opacity: 0.8;
            cursor: pointer;
          }
        `}</style>
      </div>
    );
  }
}

export default ImageLoader;
