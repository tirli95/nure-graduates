// @flow
import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete';

import t from '../../helpers/locale';
import options from './groups';

const predicate = item => item.label;
const Item = (item, isHighlighted) => (
  <div className="option" style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
    {item.label}
    <style jsx>{`
      .option {
        padding: 0.25rem 0.5rem;
      }
    `}</style>
  </div>
);

type GroupInputPropsType = {
  defaultValue?: string,
  value: string,
  name: string,
}
class GroupInput extends Component {
  defaultProps: {
    value: '',
    defaultValue: '',
  }
  state = { groupCode: '' }
  props: GroupInputPropsType

  handleChanges = () => {
    this.setState({
      group: `${this.state.groupCode}-${this.groupDate.value}-${this.groupNumber.value}`,
    });
  }

  onAutocompleteChange = (event, value) => this.onAutocompleteSelect(value)
  onAutocompleteSelect = (value) => {
    this.setState({ groupCode: value }, () => this.handleChanges());
  }

  render() {
    const { value, name } = this.props;

    return (
      <div className="group">
        <Autocomplete
          value={this.state.groupCode}
          items={options}
          inputProps={{ type: 'text', placeholder: t('groupCode') }}
          onChange={this.onAutocompleteChange}
          onSelect={this.onAutocompleteSelect}
          getItemValue={predicate}
          renderItem={Item}
        />
        &nbsp;-&nbsp;
        <input
          type="number"
          required
          className="group-input"
          placeholder={t('groupDate')}
          ref={(e) => { this.groupDate = e; }}
          onChange={this.handleChanges}
        />
        &nbsp;-&nbsp;
        <input
          type="number"
          required
          className="group-input"
          placeholder={t('groupNumber')}
          ref={(e) => { this.groupNumber = e; }}
          onChange={this.handleChanges}
        />
        <input type="text" name={name} defaultValue={value} value={this.state.group} readOnly className="display-input" />

        <style jsx>{`
          .group {
            display: flex;
            flex-direction: row;
            flex: 1;
            align-items: center;
          }

          .group-input {
            width: 33%;
          }

          .display-input {
            background: transparent;
            border: none;
            width: 4rem;
          }
        `}</style>

      </div>
    );
  }

}

export default GroupInput;

