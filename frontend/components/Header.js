// @flow
import Link from 'next/link';
import { set as setCookie } from 'js-cookie';

import t, { getLang } from '../helpers/locale';
import { LIGHT_BLUE_ACCENT_COLOR, RED_ACCENT_COLOR } from '../constants';
import Avatar from '../components/Avatar';

type HeaderPropType = {
  user?: Object,
  onLogout: Function,
};

function handleLocale(lang) {
  setCookie('lang', lang);
  window.location.reload();
}

const Header = ({ user, onLogout }: HeaderPropType) => {
  const lang = getLang();
  return (
    <header className="header">
      <div className="firstLine">
        <div className="logo">
          <Link href="/">
            <img src="/static/images/nure.png" alt="ХНУРЭ логотип" />
          </Link>
          <span className="title">
            {t('title')}
          </span>
        </div>

        <div className="rightContainer">
          <div className="langContainer" >
            <button
              onClick={() => handleLocale('ru')}
              className={lang === 'ru' ? 'lang active' : 'lang'}
            >
              RU
            </button>
            <button
              onClick={() => handleLocale('uk')}
              className={lang === 'uk' ? 'lang active' : 'lang'}
            >
              UK
            </button>
          </div>
          {user && (
            <div className="authInfo">
              <a href="/logout" className="register" onClick={onLogout}>{t('logout')}</a>
              <Link href="/personal">
                <a>
                  <Avatar
                    img={user.photo}
                    style={{
                      width: '2rem',
                      height: '2rem',
                      marginLeft: '1rem',
                      borderRadius: '3rem',
                    }}
                  />
                </a>
              </Link>
            </div>
          )}
          {!user && (
            <div className="authInfo">
              <Link href="/login"><a className="register">{t('login')}</a></Link>
              /
            <Link href="/signup"><a className="register">{t('register')}</a></Link>
            </div>
          )}
        </div>
      </div>
      <nav >
        <ul className="tabs">
          <li><Link href="/news " className="tab">
            <a className="tab">{t('news')}</a>
          </Link></li>
          <li><Link href="/international " className="tab">
            <a className="tab">{t('international')}</a>
          </Link></li>
          <li><Link href="/journal " className="tab">
            <a className="tab">{t('journal')}</a>
          </Link></li>
          <li><Link href="/graduates " className="tab">
            <a className="tab">{t('graduates')}</a>
          </Link></li>
          <li><Link href="/cooperation " className="tab">
            <a className="tab">{t('cooperation')}</a>
          </Link></li>
        </ul>
      </nav>


      <style jsx>{`
        .header {
          display: flex;
          align-self: center;
          flex-direction: column;
          padding: 1rem 1rem 0 1rem;
          color: ${LIGHT_BLUE_ACCENT_COLOR};
          max-width: 60rem;
          width: 100%;
        }

        .firstLine {
          display: flex;
          justify-content: space-between;
          align-items: center;
        }
        .logo {
          font-size: 0.8rem;
          display: flex;
          align-items: center;
          width: 12rem;
        }
        .logo img{
          height: 3rem;
          margin-right: 0.5rem;
        }
        @media (max-width: 600px) {
          .title {
            display: none;
          }
        }
        .logo img:hover {
          cursor: pointer;
        }
        .rightContainer {
          display: flex;
          align-items: center;
        }
        .langContainer {
          display: flex;
          align-items: baseline;
        }
        .lang {
          border: none;
          background-color: transparent;
          cursor: pointer;
          outline: none;
          color: ${LIGHT_BLUE_ACCENT_COLOR};
        }
        .active {
          color: white;
          background-image: url(../static/images/lang-bg.gif);
          background-repeat: no-repeat;
          width: 54px;
          height: 23px;
          padding: 7px 0 0 23px;
        }
        .authInfo {
          display: flex;
          align-items: center;
          margin-left: 1rem;
        }
        .tabs {
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          align-items: center;
          margin: 1rem 0;
          list-style: none;
          padding: 0;
          flex-wrap: wrap;
          line-height: 2rem;
        }
        .tab {
          color: black;
          border-bottom: 3px solid ${LIGHT_BLUE_ACCENT_COLOR};
          padding: 0 0.5rem;
        }
        .tab:hover {
          color: ${RED_ACCENT_COLOR};
          border-color: ${RED_ACCENT_COLOR};
        }
    `}</style>
    </header>
  );
};

Header.defaultProps = {
  user: null,
};

export default Header;
