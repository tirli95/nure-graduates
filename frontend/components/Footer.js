// @flow
import { BLUE_ACCENT_COLOR, LIGHT_BLUE_ACCENT_COLOR } from '../constants';

const Footer = () => (
  <footer className="footer">
    © ХНУРЭ, 2017

    <style jsx>{`
      .footer {
        background-color: ${BLUE_ACCENT_COLOR};
        height: 1rem;
        color: white;
        font-size: 1rem;
        padding: 1rem;
        display: flex;
        align-items: center;
        border-top: 1px solid ${LIGHT_BLUE_ACCENT_COLOR};
      }
    `}</style>
  </footer>
);

export default Footer;
