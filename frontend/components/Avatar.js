// @flow
function Avatar({ img, style }: { img?: String, style?: Object }) {
  return (
    <img
      style={style}
      src={img ? (process.env.APIHOST + img) : '../static/images/avatar-placeholder.png'}
      alt="User avatar"
    />
  );
}
Avatar.defaultProps = {
  img: '',
  style: {
    width: '6rem',
    height: '6rem',
  },
};

export default Avatar;
