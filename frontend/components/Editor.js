// @flow
import React, { Component } from 'react';
import { init } from 'pell';

import Error from '../components/Error';

class Editor extends Component {
  props: { onSubmit: Fuction }
  state = {
    html: '',
    submitted: '',
    errors: null,
  }
  onSubmit = () => {
    if (this.input.reportValidity() && this.state.html) {
      this.props.onSubmit({
        content: this.state.html,
        title: this.input.value,
      })
        .then(() => this.setState({ submitted: 'done' }))
        .catch(err => this.setState({ errors: err }))
      ;
    }
  }
  onChange = (html) => {
    this.setState({ html, submitted: '' });
  }
  initPell = (e) => {
    init({
      element: e,
      onChange: this.onChange,
      actions: [
        'italic',
        'bold',
        'underline',
        'heading1',
        'heading2',
        'paragraph',
        'quote',
        'olist',
        'ulist',
        'code',
        'line',
        {
          name: 'link',
          icon: 'link',
        },
        {
          name: 'image',
          icon: 'img',
        },
      ],
    });
  }

  render() {
    return (
      <div ref={this.initPell}>
        <input type="text" className="default" placeholder="Заголовок" ref={(e) => { this.input = e; }} required />
        <button className={`default ${this.state.submitted}`} onClick={this.onSubmit}>Создать новость</button>
        <Error errors={this.state.errors} />
        <style jsx global>{`
          .pell-content {
            box-sizing: border-box;
            height: 300px;
            outline: 0;
            overflow-y: auto;
            padding: 10px;
            width: 100%;
            background: white;
          }

          .pell-actionbar {
            background-color: white;
            border-bottom: 1px solid rgba(10, 10, 10, 0.1);
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            width: 100%;
          }

          .pell-button {
            background-color: transparent;
            border: none;
            cursor: pointer;
            height: 30px;
            outline: 0;
            width: 30px;
        `}</style>
      </div>
    );
  }
}

export default Editor;
