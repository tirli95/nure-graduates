// @flow
import { RED_ACCENT_COLOR } from '../constants';
import t from '../helpers/locale';

function Error({ errors }: { errors: String|Array }) {
  return errors ? (
    <ul className="errors">
      {
        typeof errors === 'string'
          ? <li>{t(errors)}</li>
          : errors.map(e => <li key={e.path}>{t(e.message)}: {e.value}</li>)
      }
      <style jsx>{`
        .errors {
          color: ${RED_ACCENT_COLOR};
          flex-basis: 100%;
        }
      `}</style>
    </ul>
  ) : null;
}

export default Error;
