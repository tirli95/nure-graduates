// @flow
import ru from '../static/locale/ru.json';
import uk from '../static/locale/uk.json';

const translations = { ru, uk };
let language = 'ru';
let translation;
export default function getTranslation(key: string) {
  return translation[key] || key;
}

export function setLang(lang = 'ru') {
  language = lang;
  translation = translations[lang];
}

export function getLang() {
  return language;
}
