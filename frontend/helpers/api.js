import fetch from 'isomorphic-fetch';

const standartOptions = {
  mode: 'cors',
  credentials: 'include',
};

const handleErrors = (r) => {
  if (!r.ok) { return r.json().then((res) => { throw res.error || res.errors; }); }
  return r;
};

export function createUser(userForm) {
  return fetch(`${process.env.APIHOST}/users`, {
    method: 'POST',
    body: userForm,
    ...standartOptions,
  })
  .then(handleErrors)
  .then(r => r.json());
}

export function updateUser(userForm, userId) {
  return fetch(`${process.env.APIHOST}/users/${userId}`, {
    method: 'PUT',
    body: userForm,
    ...standartOptions,
  })
  .then(handleErrors)
  .then(r => r.json());
}

export function loginUser(userForm) {
  return fetch(`${process.env.APIHOST}/auth`, {
    method: 'POST',
    body: userForm,
    ...standartOptions,
  })
  .then(handleErrors)
  .then(r => r.json());
}

export function getMe() {
  return fetch(`${process.env.APIHOST}/auth/me`, {
    ...standartOptions,
  })
  .then(handleErrors)
  .then(r => r.json());
}

export function logout() {
  return fetch(`${process.env.APIHOST}/auth/logout`, {
    method: 'POST',
    ...standartOptions,
  })
  .then(handleErrors);
}

export function getUsers() {
  return fetch(`${process.env.APIHOST}/users`, {
    ...standartOptions,
  })
  .then(handleErrors)
  .then(r => r.json());
}

export function createNews(news) {
  return fetch(`${process.env.APIHOST}/news`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(news),
    ...standartOptions,
  })
  .then(handleErrors);
}

export function getNews() {
  return fetch(`${process.env.APIHOST}/news`, {
    ...standartOptions,
  })
  .then(handleErrors)
  .then(r => r.json());
}
