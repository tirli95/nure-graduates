// @flow
import { parse } from 'cookie';

import { setLang } from './locale';

export default function ({ req } = {}) {
  const { lang } = req ? parse(req.headers.cookie || '') : parse(document.cookie || '');
  setLang(lang);
  return {};
}
