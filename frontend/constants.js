export const MAIN_COLOR = '#f6f1e3';
export const LIGHT_BLUE_ACCENT_COLOR = '#0088cc';
export const BLUE_ACCENT_COLOR = '#004466';
export const RED_ACCENT_COLOR = '#b13533';
