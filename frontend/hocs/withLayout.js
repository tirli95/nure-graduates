// @flow
import React from 'react';
import Head from 'next/head';
import { compose, withHandlers } from 'recompose';

import Header from '../components/Header';
import Footer from '../components/Footer';
import t from '../helpers/locale';
import { logout } from '../helpers/api';
import { MAIN_COLOR, LIGHT_BLUE_ACCENT_COLOR, RED_ACCENT_COLOR, BLUE_ACCENT_COLOR } from '../constants';

type LayoutPropType = {
  user?: Object,
  setUser: Function,
  onLogout: Function,
  rest: Object,
};

const withLogoutHandlers = withHandlers({
  onLogout: ({ setUser }) => (event) => {
    event.preventDefault();
    logout().then(() => setUser(null));
  },
});

function Layout(WrappedComponent) {
  function layoutHOC({ user, setUser, onLogout, ...rest }: LayoutPropType) {
    return (
      <main className="main">
        <Head>
          <title>{t('title')}</title>
          <meta charSet="utf-8" />
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <meta name="theme-color" content="#004466" />
          <link rel="manifest" href="/static/manifest.json" />
          <link rel="icon" type="image/png" sizes="96x96" href="/static/images/favicon-96x96.png" />
          {/* Needed for avatar crop :( */}
          <link rel="stylesheet" href="/static/bootstrap.css" />
        </Head>

        <Header user={user} onLogout={onLogout} />
        <section className="content">
          <WrappedComponent {...rest} setUser={setUser} user={user} />
        </section>
        <Footer />

        <style jsx>{`
          .main {
            background-color: ${MAIN_COLOR};
            display: flex;
            flex-direction: column;
            min-height: 100vh;
          }
          .content {
            display: flex;
            align-self: center;
            flex: 1;
            padding: 0 1rem 1rem;
            max-width: 60rem;
            width: 100%;
          }
        `}</style>

        <style jsx global>{`
          html {
            box-sizing: border-box;
            font-size: 1.25rem;
          }

          html * {
            box-sizing: inherit;
          }

          body {
            margin: 0;
            padding: 0;
          }
          a {
            color: ${LIGHT_BLUE_ACCENT_COLOR};
            text-decoration: none;
            font-size: 1rem;
          }
          a:hover {
            color: ${RED_ACCENT_COLOR};
          }

          input, textarea {
            height: 2rem;
            padding: 0 0.5rem;
          }
          input[type='text'], input[type='password'], input[type='number'], textarea {
            border: 1px solid ${LIGHT_BLUE_ACCENT_COLOR};
          }
          textarea {
            height: 4rem;
          }
          button.default {
            height: 2rem;
            background: linear-gradient(110deg, ${LIGHT_BLUE_ACCENT_COLOR}, ${BLUE_ACCENT_COLOR});
            color: white;
            border: 1px solid ${BLUE_ACCENT_COLOR};
            flex-basis: 100%;
          }
          button.default.done:after {
            content: "✓";
            color: greenyellow;
            padding-left: 0.5rem;
          }
          h1 {
            font-size: 1.4rem;
          }
          h2 {
            font-size: 1.1rem;
          }
          .staticPage ul {
            list-style: none;
            padding-left: 1rem;
          }

          .photo-container {
            text-align: center;
            clear: both;
          }

          .photo-container.left {
            float: left;
            margin: 1rem 2rem 1rem 0;
          }

          .photo-container.right {
            float: right;
            margin: 1rem 0 1rem 2rem;
          }

          .photo-container img {
            max-width: 15rem;
            min-width: 10rem;
            width: 100%;
          }

          .photo-container figcaption {
            font-family: monospace;
          }

          .icon-link:after {
            content: '';
            display: inline-block;
            width: 0.8rem;
            height: 0.8rem;
            background-image: url('/static/images/link.svg');
            background-repeat: no-repeat;
            background-size: contain;
            margin-left: 0.2rem;
          }
        `}</style>
      </main>
    );
  }

  layoutHOC.defaultProps = {
    user: null,
  };

  return compose(withLogoutHandlers)(layoutHOC);
}

export default Layout;
