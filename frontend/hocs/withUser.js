import { compose, withState, lifecycle } from 'recompose';
import { getMe } from '../helpers/api';
import handleLang from '../helpers/handleLang';

const withUserState = withState('user', 'setUser', ({ user }) => user);
const withLifecycle = lifecycle({
  componentDidMount() {
    getMe().then(this.props.setUser.bind(this));
  },
  componentWillMount() {
    if (typeof document !== 'undefined') {
      handleLang();
    }
  },
});

function withUser(WrappedComponent) {
  function userHOC(props) {
    return <WrappedComponent {...props} />;
  }

  return compose(withUserState, withLifecycle)(userHOC);
}

export default withUser;
