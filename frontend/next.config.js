const webpack = require('webpack');

module.exports = {
  webpack: (config) => {
    const envPlugin = new webpack.EnvironmentPlugin(['APIHOST']);
    config.plugins.push(envPlugin);

    return config;
  },
};
