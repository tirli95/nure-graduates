// @flow
import { compose } from 'recompose';

import ru from '../static/pages/ru/history.html';
import uk from '../static/pages/uk/history.html';
import withLayout from '../hocs/withLayout';
import withUser from '../hocs/withUser';
import handleLang from '../helpers/handleLang';
import { getLang } from '../helpers/locale';

const translations = { ru, uk };

const Index = () => {
  const lang = getLang();
  return (
    <div dangerouslySetInnerHTML={{ __html: translations[lang] }} />
  );
};

const wrappedIndex = compose(withUser, withLayout)(Index);
wrappedIndex.getInitialProps = handleLang;

export default wrappedIndex;
