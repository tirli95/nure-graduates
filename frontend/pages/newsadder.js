// @flow
import { compose } from 'recompose';

import withUser from '../hocs/withUser';
import withLayout from '../hocs/withLayout';
import { createNews } from '../helpers/api';
import handleLang from '../helpers/handleLang';
import Editor from '../components/Editor';

const NewsAdder = () => (
  <section>
    <Editor onSubmit={createNews} />
    <style jsx>{`
      section {
        width: 100%;
      }
    `}</style>
  </section>
);

NewsAdder.defaultProps = {
  user: undefined,
  error: '',
};

const wrappedNewsAdder = compose(
  withUser,
  withLayout,
)(NewsAdder);

wrappedNewsAdder.getInitialProps = handleLang;

export default wrappedNewsAdder;
