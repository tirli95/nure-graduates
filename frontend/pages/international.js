// @flow
import { compose } from 'recompose';

import ru from '../static/pages/ru/international.html';
import uk from '../static/pages/uk/international.html';
import withLayout from '../hocs/withLayout';
import withUser from '../hocs/withUser';
import handleLang from '../helpers/handleLang';
import { getLang } from '../helpers/locale';

const translations = { ru, uk };

const International = () => {
  const lang = getLang();
  return (
    <div dangerouslySetInnerHTML={{ __html: translations[lang] }} />
  );
};

const wrappedInternational = compose(withUser, withLayout)(International);
wrappedInternational.getInitialProps = handleLang;

export default wrappedInternational;
