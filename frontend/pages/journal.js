// @flow
import { compose } from 'recompose';

import ru from '../static/pages/ru/journal.html';
import uk from '../static/pages/uk/journal.html';
import withLayout from '../hocs/withLayout';
import withUser from '../hocs/withUser';
import handleLang from '../helpers/handleLang';
import { getLang } from '../helpers/locale';

const translations = { ru, uk };

const Journal = () => {
  const lang = getLang();
  return (
    <div dangerouslySetInnerHTML={{ __html: translations[lang] }} />
  );
};

const wrappedJournal = compose(withUser, withLayout)(Journal);
wrappedJournal.getInitialProps = handleLang;

export default wrappedJournal;
