// @flow
import { compose } from 'recompose';

import ru from '../static/pages/ru/cooperation.html';
import uk from '../static/pages/uk/cooperation.html';
import withLayout from '../hocs/withLayout';
import withUser from '../hocs/withUser';
import handleLang from '../helpers/handleLang';
import { getLang } from '../helpers/locale';

const cooperations = { ru, uk };

const Сooperation = () => {
  const lang = getLang();
  return (
    <div dangerouslySetInnerHTML={{ __html: cooperations[lang] }} />
  );
};

const wrappedCooperation = compose(withUser, withLayout)(Сooperation);
wrappedCooperation.getInitialProps = handleLang;

export default wrappedCooperation;
