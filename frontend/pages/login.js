// @flow
import { compose, withHandlers, withState } from 'recompose';
import Router from 'next/router';

import Error from '../components/Error';
import withUser from '../hocs/withUser';
import withLayout from '../hocs/withLayout';
import t from '../helpers/locale';
import { loginUser } from '../helpers/api';
import handleLang from '../helpers/handleLang';

const withErrorState = withState('error', 'setError', null);
const withLoginHandlers = withHandlers({
  onSubmit: ({ setUser, setError }) => (event) => {
    event.preventDefault();
    const form = new FormData(event.target);
    loginUser(form)
      .then(setUser)
      .then(() => Router.push('/'))
      .catch(setError)
    ;
  },
});

type LoginPropsType = {
  onSubmit: Function,
  error?: String|Array,
}

const Login = ({ onSubmit, error }: LoginPropsType) => (
  <section>
    <h2>{t('login')}</h2>
    <form className="loginForm" onSubmit={onSubmit}>
      <label htmlFor="email">
        <span className="labelText">{t('email')}</span>
        <input type="text" id="email" name="email" />
      </label>
      <label htmlFor="password">
        <span className="labelText">{t('password')}</span>
        <input type="password" id="password" name="password" />
      </label>
      <Error errors={error} />
      <button className="default" type="submit">{t('submit')}</button>
    </form>

    <style jsx>{`
      .loginForm {
        display: flex;
        flex-direction: column;
      }
      label {
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding-bottom: 1rem;
        width: 100%;
      }
      .labelText {
        display: block;
        width: 10rem;
      }
      input {
        flex: 1;
      }
    `}</style>
  </section>
);

Login.defaultProps = {
  user: undefined,
  error: '',
};

const wrappedLogin = compose(
  withUser,
  withLayout,
  withErrorState,
  withLoginHandlers,
)(Login);

wrappedLogin.getInitialProps = handleLang;

export default wrappedLogin;
