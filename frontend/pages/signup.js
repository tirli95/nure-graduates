// @flow
import { compose, withState, withHandlers } from 'recompose';
import Router from 'next/router';

import t from '../helpers/locale';
import { createUser } from '../helpers/api';
import handleLang from '../helpers/handleLang';
import b64toBlob from '../helpers/b64toBlob';
import withLayout from '../hocs/withLayout';
import withUser from '../hocs/withUser';
import Error from '../components/Error';
import GroupInput from '../components/GroupInput/GroupInput';
import ImageLoader from '../components/ImageLoader';

const withErrorState = withState('error', 'setError', null);
const withAvatarState = withState('avatar', 'onAvatarSelect', null);
const withSignupHandlers = withHandlers({
  onSubmit: ({ setUser, setError, avatar }) => (event) => {
    event.preventDefault();
    const form = new FormData(event.target);
    if (avatar) {
      form.append('photo', b64toBlob(avatar));
    }
    createUser(form)
      .then(setUser)
      .then(() => Router.push('/'))
      .catch(setError);
  },
});


type SignUpPropsType = {
  onSubmit: Function,
  error?: string|Array,
  onAvatarSelect: Function,
}

const SignUp = ({ onSubmit, error, onAvatarSelect }: SignUpPropsType) => (
  <section>
    <h2>{t('register')}</h2>
    <form className="signupForm" onSubmit={onSubmit}>
      <label htmlFor="email">
        <span className="labelText">{t('email')}*</span>
        <input type="text" id="email" name="email" required />
      </label>
      <label htmlFor="password">
        <span className="labelText">{t('password')}*</span>
        <input type="password" id="password" name="password" required />
      </label>
      <label htmlFor="name">
        <span className="labelText">{t('name')}*</span>
        <input type="text" id="name" name="name" required />
      </label>
      <label htmlFor="surname">
        <span className="labelText">{t('surname')}*</span>
        <input type="text" id="surname" name="surname" required />
      </label>
      <label htmlFor="surnameNow">
        <span className="labelText">{t('surnameNow')}</span>
        <input type="text" id="surnameNow" name="surnameNow" />
      </label>
      <label htmlFor="patronymic">
        <span className="labelText">{t('patronymic')}</span>
        <input type="text" id="patronymic" name="patronymic" />
      </label>
      <label htmlFor="group">
        <span className="labelText">{t('group')}*</span>
        <GroupInput name="group" />
      </label>
      <label htmlFor="phone">
        <span className="labelText">{t('phone')}</span>
        <input type="text" id="phone" name="phone" placeholder="+380991234567" />
      </label>
      <label htmlFor="fb">
        <span className="labelText">{t('fb')}</span>
        <input type="text" id="fb" name="fb" placeholder="https:/www.facebook.com/example" />
      </label>
      <label htmlFor="linkedin">
        <span className="labelText">{t('linkedin')}</span>
        <input type="text" id="linkedin" name="linkedin" placeholder="https:/www.linkedin.com/example" />
      </label>
      <label htmlFor="photo">
        <span className="labelText">{t('photo')}</span>
        <ImageLoader onSelect={onAvatarSelect} />
      </label>

      <label htmlFor="info">
        <span className="labelText">{t('info')}</span>
        <textarea type="text" id="info" name="info" />
      </label>

      <Error errors={error} />

      <button className="default" type="submit">{t('submit')}</button>
    </form>

    <style jsx>{`
      .signupForm {
        display: flex;
        flex-flow: row wrap;
      }
      label {
        display: flex;
        align-items: center;
        padding: 0 1rem 1rem;
        width: 100%;
        flex: 1 0 50%;
      }
      .labelText {
        display: block;
        width: 10rem;
      }
      input, textarea {
        flex: 1;
      }
    `}</style>
  </section>
);

SignUp.defaultProps = {
  error: '',
};

const wrappedSignup = compose(
  withUser,
  withLayout,
  withAvatarState,
  withErrorState,
  withSignupHandlers,
)(SignUp);

wrappedSignup.getInitialProps = handleLang;

export default wrappedSignup;
