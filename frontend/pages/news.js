// @flow
import { compose } from 'recompose';
import Link from 'next/link';
import format from 'date-fns/format';
import compareDesc from 'date-fns/compare_desc';

import withLayout from '../hocs/withLayout';
import withUser from '../hocs/withUser';
import handleLang from '../helpers/handleLang';
import { getNews } from '../helpers/api';
import t from '../helpers/locale';

type PostType = {
  date: Date,
  content: string,
  title: string,
};
const Post = ({ info }: { info: PostType }) => (
  <article>
    <header>
      <h1>{info.title}</h1>
      <time dateTime={info.date}>{format(info.date, 'DD.MM.YYYY hh:mm')}</time>
    </header>

    <p dangerouslySetInnerHTML={{ __html: info.content }} />
    <style jsx>{`
      article {
        border-bottom: 1px solid lightgray;
        background: rgba(255, 255, 255, 0.5);
        border-radius: 0.25rem;
        padding: 0 1rem;
        margin: 1rem 0;
      }
      header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #ddd;
      }
      h1 {
        font-weight: 400;
      }
      time {
        font-size: 0.7rem;
        color: grey;
      }
    `}</style>
  </article>
);

type NewsPropsType = {
  user: Object,
  news: Array<PostType>,
};

const News = ({ user, news }: NewsPropsType) => (
  <div className="news">
    <h1>{t('news')}</h1>
    {
      user && user.type === 'Admin' &&
      <Link href="/newsadder"><a className="register">Добавить новость</a></Link>
    }
    {
      news
        .sort((a, b) => compareDesc(a.date, b.date))
        .map(n => <Post info={n} key={n.date} />)
    }
    <style jsx>{`
      .news {
        width: 100%;
      }
    `}</style>
  </div>
);

News.defaultProps = {
  user: null,
  news: [],
};

const wrappedNews = compose(withUser, withLayout)(News);

wrappedNews.getInitialProps = async (context) => {
  await handleLang(context);
  const news = await getNews();
  return { news };
};
export default wrappedNews;
