// @flow
import { compose, withState, withHandlers } from 'recompose';
import icons from 'svg-social-icons';

import { getUsers } from '../helpers/api';
import handleLang from '../helpers/handleLang';
import withLayout from '../hocs/withLayout';
import withUser from '../hocs/withUser';
import t from '../helpers/locale';
import Avatar from '../components/Avatar';

const UserCard = ({ user }: { user: Object }) => (
  <div className="card" key={user._id}>
    <Avatar
      img={user.photo}
      style={{
        width: '6rem',
        height: '6rem',
        marginRight: '1rem',
      }}
    />
    <section className="info">
      <p><strong>
        {user.surname} {user.surnameNow && `(${user.surnameNow})`} {user.name} {user.patronymic}
      </strong></p>
      <p><sup>{user.group}</sup></p>
      <p>{user.email && `Email: ${user.email}`}</p>
      <p>{user.phone && `${t('phone')}: ${user.phone}`}</p>
      <p>
        {user.fb &&
          <a
            href={user.fb}
            target="_blank"
            rel="noopener noreferrer"
            dangerouslySetInnerHTML={{ __html: icons('facebook') }}
          />
        }
        {user.linkedin &&
          <a
            href={user.linkedin}
            target="_blank"
            rel="noopener noreferrer"
            dangerouslySetInnerHTML={{ __html: icons('linkedin') }}
          />
        }
      </p>
    </section>

    <style jsx>{`
      :global(.social-svg) {
        height: 1rem;
        margin-right: 0.5rem;
      }
      :global(.social-svg-icon) {
        fill: white;
      }
      .card {
        display: flex;
        align-items: center;
        padding: 0.5rem 0;
      }
      p {
        margin: 0;
      }
    `}</style>
  </div>
);

const withUsersState = withState('filteredUsers', 'updateUsers', ({ users }) => users);
const withGraduatesHandlers = withHandlers({
  onSearch: ({ updateUsers, users }) => (event) => {
    event.preventDefault();
    const search = event.target.search.value;

    const user = users.filter(u =>
      Object.values(u).find(value => value.includes && value.includes(search)),
    );
    updateUsers(user);
  },
});

const Graduates = ({ filteredUsers, onSearch }: { filteredUsers: Array, onSearch: Function }) => (
  <section>
    <form onSubmit={onSearch}>
      <label htmlFor="search">
        <input type="text" name="search" />
      </label>
      <button className="default">{t('search')}</button>
    </form>
    {filteredUsers.map(u => <UserCard user={u} key={u._id} />)}
  </section>
);

Graduates.defaultProps = {
  users: [],
};

const wrappedGraduates = compose(
  withUser,
  withUsersState,
  withGraduatesHandlers,
  withLayout,
)(Graduates);

wrappedGraduates.getInitialProps = async (context) => {
  await handleLang(context);
  const users = await getUsers();
  return { users };
};

export default wrappedGraduates;
