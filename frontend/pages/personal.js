// @flow
import { compose, withState, withHandlers, lifecycle } from 'recompose';
import Router from 'next/router';

import Error from '../components/Error';
import ImageLoader from '../components/ImageLoader';
import withUser from '../hocs/withUser';
import withLayout from '../hocs/withLayout';
import t from '../helpers/locale';
import { updateUser } from '../helpers/api';
import b64toBlob from '../helpers/b64toBlob';
import handleLang from '../helpers/handleLang';

const withErrorState = withState('error', 'setError', null);
const withAvatarState = withState('avatar', 'onAvatarSelect', null);
const withLifecycle = lifecycle({
  componentWillReceiveProps(props) {
    if (!props.user) {
      Router.push('/');
    }
  },
});
const withPersonalHandlers = withHandlers({
  onSubmit: ({ setUser, setError, user, avatar }) => (event) => {
    event.preventDefault();
    const form = new FormData(event.target);
    if (avatar) {
      form.append('photo', b64toBlob(avatar));
    }
    updateUser(form, user._id)
      .then(setUser)
      .catch(setError);
  },
});

type PersonalPropsType = {
  user: Object,
  error?: string | Array,
  onSubmit: Function,
  onAvatarSelect: Function,
}

const Personal = ({ user, onSubmit, error, onAvatarSelect }: PersonalPropsType) => (
  user ?
    <section>
      <h2>{t('personal')}</h2>
      <form className="userForm" onSubmit={onSubmit}>
        <label htmlFor="name">
          <span className="labelText">{t('name')}*</span>
          <input type="text" id="name" name="name" required defaultValue={user.name} />
        </label>
        <label htmlFor="surname">
          <span className="labelText">{t('surname')}*</span>
          <input type="text" id="surname" name="surname" required defaultValue={user.surname} />
        </label>
        <label htmlFor="surnameNow">
          <span className="labelText">{t('surnameNow')}</span>
          <input type="text" id="surnameNow" name="surnameNow" defaultValue={user.surnameNow} />
        </label>
        <label htmlFor="patronymic">
          <span className="labelText">{t('patronymic')}</span>
          <input type="text" id="patronymic" name="patronymic" defaultValue={user.patronymic} />
        </label>
        <label htmlFor="group">
          <span className="labelText">{t('group')}*</span>
          <input type="text" id="group" name="group" required defaultValue={user.group} />
        </label>
        <label htmlFor="phone">
          <span className="labelText">{t('phone')}</span>
          <input type="text" id="phone" name="phone" placeholder="+380991234567" defaultValue={user.phone} />
        </label>
        <label htmlFor="fb">
          <span className="labelText">{t('fb')}</span>
          <input type="text" id="fb" name="fb" placeholder="https:/www.facebook.com/example" defaultValue={user.fb} />
        </label>
        <label htmlFor="linkedin">
          <span className="labelText">{t('linkedin')}</span>
          <input type="text" id="linkedin" name="linkedin" placeholder="https:/www.linkedin.com/example" defaultValue={user.linkedin} />
        </label>
        <label htmlFor="photo">
          <span className="labelText">{t('photo')}</span>
          <ImageLoader value={user.photo} onSelect={onAvatarSelect} />
        </label>
        <label htmlFor="info">
          <span className="labelText">{t('info')}</span>
          <textarea type="text" id="info" name="info" defaultValue={user.info} />
        </label>

        <Error errors={error} />

        <button className="default" type="submit">{t('submit')}</button>
      </form>
      <style jsx>{`
        .userForm {
          display: flex;
          flex-flow: row wrap;
        }
        label {
          display: flex;
          align-items: center;
          padding: 0 1rem 1rem;
          width: 100%;
          flex: 1 0 50%;
        }
        .labelText {
          display: block;
          width: 10rem;
        }
        input, textarea {
          flex: 1;
        }
    `}</style>
    </section>
    : null
);

Personal.defaultProps = {
  error: null,
};

const wrappedPersonal = compose(
  withUser,
  withLayout,
  withErrorState,
  withAvatarState,
  withLifecycle,
  withPersonalHandlers,
)(Personal);

wrappedPersonal.getInitialProps = handleLang;

export default wrappedPersonal;
