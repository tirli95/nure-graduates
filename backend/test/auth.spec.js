import supertest from 'supertest';
import { expect, should } from 'chai';
import faker from 'faker';

import { cleanDb } from './utils';
import { createUser, getAuthToken } from './fixtures/users';
import app from '../bin/server';

should();
const request = supertest.agent(app.listen());
const context = {};

describe('Auth', () => {
  before(async () => {
    cleanDb();
    context.credentials = {
      email: faker.internet.email(),
      password: 'foopass',
      name: 'foo',
      surname: 'bar',
      group: ['КН-12-4'],
    };
    context.user = await createUser(context.credentials);
    context.token = await getAuthToken(request, context.credentials);
  });

  describe('POST /auth', () => {
    it('should throw 401 if credentials are incorrect', (done) => {
      request
        .post('/auth')
        .set('Accept', 'application/json')
        .send({ email: 'supercoolname', password: 'wrongpassword' })
        .expect(401, done);
    });

    it('should auth user', (done) => {
      request
        .post('/auth')
        .set('Accept', 'application/json')
        .send(context.credentials)
        .expect(200, (err, res) => {
          if (err) { return done(err); }

          res.body.user.should.have.property('email');
          res.body.user.email.should.equal(context.credentials.email);
          expect(res.body.user.password).to.not.exist;

          return done();
        });
    });
  });
});
