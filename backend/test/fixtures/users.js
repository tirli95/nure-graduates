import faker from 'faker';

import User from '../../src/models/users';

export function createUser(attributes) {
  const newUser = Object.assign({
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
  }, attributes);

  const user = new User(newUser);
  return user.save();
}

export function getAuthToken(agent, { email, password }) {
  return new Promise((resolve, reject) => {
    agent
    .post('/auth')
    .set('Accept', 'application/json')
    .send({ email, password })
    .end((err, res) => {
      if (err) { return reject(err); }
      return resolve(res.body.token);
    });
  });
}
