import supertest from 'supertest';
import { expect, should } from 'chai';
import faker from 'faker';

import app from '../bin/server';
import { cleanDb } from './utils';
import { createUser, getAuthToken } from './fixtures/users';

should();
const request = supertest.agent(app.listen());
const context = {};

describe('Users', () => {
  beforeEach(async () => {
    await cleanDb();

    context.credentials = {
      email: faker.internet.email(),
      password: 'foopass',
      name: 'foo',
      surname: 'bar',
      group: ['КН-12-4'],
    };

    context.user = await createUser(context.credentials);
    context.token = await getAuthToken(request, context.credentials);
  });


  describe('POST /users', () => {
    it('should reject signup when data is incomplete', (done) => {
      request
        .post('/users')
        .type('form')
        .set('Accept', 'multipart/form-data')
        .send({ email: faker.internet.email() })
        .expect(422, done);
    });

    it('should sign up with minimal requirements', (done) => {
      const userData = {
        user: Object.assign({}, context.credentials, {
          email: 'newemail@mail.com',
        }),
      };
      request
        .post('/users')
        .set('Accept', 'multipart/form-data')
        .send(userData)
        .expect(200, (err, res) => {
          if (err) { return done(err); }

          res.body.user.should.have.property('email');
          res.body.user.email.should.equal(userData.user.email);
          expect(res.body.user.password).to.not.exist;

          return done();
        });
    });

    it('should reject new sign up with same email', (done) => {
      request
        .post('/users')
        .set('Accept', 'multipart/form-data')
        .send({ user: context.credentials })
        .expect(422, done);
    });

    it('should sign up with all requirements', (done) => {
      const userData = {
        user: Object.assign({}, context.credentials, {
          email: 'newemail2@mail.com',
          patronymic: 'patrotest',
          phone: '+380661234567',
          fb: 'https://fb.com/1',
          linkedin: 'https://linkedin.com/',
          info: 'more info here',
          photo: '/link/to/img.png',
        }),
      };
      request
        .post('/users')
        .set('Accept', 'multipart/form-data')
        .send(userData)
        .expect(200, (err, res) => {
          if (err) { return done(err); }

          res.body.user.should.have.property('email');
          res.body.user.email.should.equal(userData.user.email);
          expect(res.body.user.password).to.not.exist;

          return done();
        });
    });
  });

  describe('GET /users', () => {
    it('should not fetch users if the authorization header is missing', (done) => {
      request
        .get('/users')
        .set('Accept', 'application/json')
        .expect(401, done);
    });

    it('should not fetch users if the authorization header is missing the scheme', (done) => {
      request
        .get('/users')
        .set({
          Accept: 'application/json',
          Authorization: '1',
        })
        .expect(401, done);
    });

    it('should not fetch users if the authorization header has invalid scheme', (done) => {
      const { token } = context;
      request
        .get('/users')
        .set({
          Accept: 'application/json',
          Authorization: `Unknown ${token}`,
        })
        .expect(401, done);
    });

    it('should not fetch users if token is invalid', (done) => {
      request
        .get('/users')
        .set({
          Accept: 'application/json',
          Authorization: 'Bearer 1',
        })
        .expect(401, done);
    });

    it('should fetch all users', (done) => {
      const { token } = context;
      request
        .get('/users')
        .set({
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        })
        .expect(200, (err, res) => {
          if (err) { return done(err); }

          res.body.should.have.property('users');

          res.body.users.should.have.length(1);

          return done();
        });
    });
  });

  describe('GET /users/:id', () => {
    it('should not fetch user if token is invalid', (done) => {
      request
        .get('/users/1')
        .set({
          Accept: 'application/json',
          Authorization: 'Bearer 1',
        })
        .expect(401, done);
    });

    it('should throw 404 if user doesn\'t exist', (done) => {
      const { token } = context;
      request
        .get('/users/1')
        .set({
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        })
        .expect(404, done);
    });

    it('should fetch user', (done) => {
      const {
        user: { _id },
        token,
      } = context;

      request
        .get(`/users/${_id}`)
        .set({
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        })
        .expect(200, (err, res) => {
          if (err) { return done(err); }

          res.body.should.have.property('user');

          expect(res.body.user.password).to.not.exist;

          return done();
        });
    });
  });

  describe('PUT /users/:id', () => {
    it('should not update user if token is invalid', (done) => {
      request
        .put('/users/1')
        .set({
          Accept: 'application/json',
          Authorization: 'Bearer 1',
        })
        .expect(401, done);
    });

    it('should throw 404 if user doesn\'t exist', (done) => {
      const { token } = context;
      request
        .put('/users/1')
        .set({
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        })
        .expect(404, done);
    });

    it('should update user', (done) => {
      const {
        user: { _id },
        token,
      } = context;

      request
        .put(`/users/${_id}`)
        .set({
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        })
        .send({ user: { email: 'updatedcoolemail' } })
        .expect(200, (err, res) => {
          if (err) { return done(err); }

          res.body.user.should.have.property('email');
          res.body.user.email.should.equal('updatedcoolemail');
          expect(res.body.user.password).to.not.exist;

          return done();
        });
    });
  });

  describe('DELETE /users/:id', () => {
    it('should not delete user if token is invalid', (done) => {
      request
        .delete('/users/1')
        .set({
          Accept: 'application/json',
          Authorization: 'Bearer 1',
        })
        .expect(401, done);
    });

    it('should throw 404 if user doesn\'t exist', (done) => {
      const { token } = context;
      request
        .delete('/users/1')
        .set({
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        })
        .expect(404, done);
    });

    it('should delete user', (done) => {
      const {
        user: { _id },
        token,
      } = context;

      request
        .delete(`/users/${_id}`)
        .set({
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        })
        .expect(200, done);
    });
  });
});
