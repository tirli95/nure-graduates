import mongoose from 'mongoose';

export function cleanDb() { // eslint-disable-line import/prefer-default-export
  const collections = Object.keys(mongoose.connection.collections);
  collections.forEach((collection) => {
    mongoose.connection.collections[collection].remove();
  });
}
