export function getToken(ctx) { // eslint-disable-line import/prefer-default-export
  const token = ctx.cookie && ctx.cookie['X-Auth-Token'];
  if (token) {
    return token;
  }
  return null;
}
