import { addNews, getNews } from './controller';
import { ensureUser, ensureAdmin } from '../../middleware/validators';

export const baseUrl = '/news';

export default [
  {
    method: 'GET',
    route: '/',
    handlers: [
      getNews,
    ],
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      ensureUser,
      ensureAdmin,
      addNews,
    ],
  },
];
