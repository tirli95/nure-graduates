import News from '../../models/news';

export async function addNews(ctx) {
  const { content, title } = ctx.request.body;
  const news = new News({
    content,
    title,
    date: Date.now(),
  });

  try {
    await news.save();
  } catch (err) {
    ctx.throw(422, err);
  }

  ctx.status = 201;
}

export async function getNews(ctx) {
  const news = await News.find({});
  ctx.body = news;
}

