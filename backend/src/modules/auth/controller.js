import passport from 'koa-passport';
import asyncBusboy from 'async-busboy';
import User from '../../models/users';

export async function authUser(ctx, next) {
  const { fields } = await asyncBusboy(ctx.req);
  ctx.request.body = fields; // needed for passport
  return passport.authenticate('local', (err, user) => {
    if (!user) {
      ctx.throw(401, err);
    }

    const token = user.generateToken();
    const response = user.toJSON();
    delete response.password;

    ctx.cookies.set('X-Auth-Token', token, { httpOnly: true });
    ctx.body = {
      token,
      user: response,
    };
  })(ctx, next);
}

export async function getMe(ctx) {
  const user = await User.findById(ctx.state.userId, '-password');

  ctx.body = user;
}

export async function logout(ctx) {
  ctx.cookies.set('X-Auth-Token', null, { httpOnly: true, expires: new Date(0) });
  ctx.status = 204;
  ctx.body = {};
}
