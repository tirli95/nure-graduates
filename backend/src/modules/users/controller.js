import asyncBusboy from 'async-busboy';
import fs from 'fs';
import uuid from 'uuid/v4';
import User from '../../models/users';

const CURRENT_DIR = `${__dirname}/../../../public/`;

export async function createUser(ctx) {
  const { files, fields } = await asyncBusboy(ctx.req);
  const { filename, fieldname } = files[0] || {};

  let filepath;
  if (filename) {
    filepath = `/images/${uuid()}_${filename}`;
    const writable = fs.createWriteStream(`${CURRENT_DIR}${filepath}`);
    files[0].pipe(writable);
  }

  const user = new User(Object.assign({}, fields, { [fieldname]: filepath }));

  try {
    await user.save();
  } catch (err) {
    ctx.throw(422, err);
  }

  const token = user.generateToken();
  const response = user.toJSON();

  delete response.password;

  ctx.cookies.set('X-Auth-Token', token, { httpOnly: true });
  ctx.body = {
    user: response,
    token,
  };
}

export async function getUsers(ctx) {
  const users = await User.find({ type: { $ne: 'Admin' } }, '-password');
  ctx.body = users;
}

export async function getUser(ctx, next) {
  try {
    const user = await User.findById(ctx.params.id, '-password');
    if (!user) {
      ctx.throw(404);
    }

    ctx.body = {
      user,
    };
  } catch (err) {
    if (err === 404 || err.name === 'CastError') {
      ctx.throw(404);
    }

    ctx.throw(500);
  }

  if (next) { return next(); }

  return undefined;
}

export async function updateUser(ctx) {
  const { files, fields } = await asyncBusboy(ctx.req);
  const { filename, fieldname } = files[0] || {};

  const avatar = {};
  if (filename) {
    avatar[fieldname] = `/images/${uuid()}_${filename}`;
    const writable = fs.createWriteStream(`${CURRENT_DIR}${avatar[fieldname]}`);
    files[0].pipe(writable);
  }

  const user = ctx.body.user;
  fs.unlink(`${CURRENT_DIR}${user.photo}`, err => err && console.error('Cannot delete old user avatar', err));
  Object.assign(user, fields, avatar);

  try {
    await user.save();
  } catch (err) {
    ctx.throw(422, err);
  }
  const response = user.toJSON();

  ctx.body = response;
  ctx.status = 200;
}

export async function deleteUser(ctx) {
  const user = ctx.body.user;

  await user.remove();

  ctx.status = 200;
  ctx.body = {
    success: true,
  };
}
