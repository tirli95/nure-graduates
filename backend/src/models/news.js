import mongoose from 'mongoose';

const News = new mongoose.Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  date: { type: Date, required: true },
});


export default mongoose.model('news', News);
