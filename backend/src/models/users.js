import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import uniqueValidator from 'mongoose-unique-validator';

import config from '../../config';

function validateGroup(arr) {
  if (!Array.isArray(arr)) return false;

  return arr.every(val => /^\S+-\d+-\d+$/.test(val));
}

const User = new mongoose.Schema({
  type: { type: String, default: 'User' },
  name: { type: String, required: true },
  surname: { type: String, required: true },
  surnameNow: { type: String },
  patronymic: { type: String },
  email: {
    type: String,
    required: true,
    unique: true,
    match: [/^\S+@\S+$/, 'INCORRECT_EMAIL'],
  },
  password: { type: String, required: true },
  group: {
    type: [String],
    required: true,
    validate: {
      validator: validateGroup,
      message: 'INCORRECT_GROUP_PATTERN',
    },
  },
  phone: { type: String, match: [/^\+[0-9]{9,15}$/, 'INCORRECT_PHONE'] },
  fb: { type: String },
  linkedin: { type: String },
  info: { type: String },
  photo: { type: String },
});

User.plugin(uniqueValidator, { message: 'NOT_UNIQUE_{PATH}' });

User.pre('save', function preSave(next) {
  const user = this;

  if (!user.isModified('password')) {
    return next();
  }

  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) { return reject(err); }
      return resolve(salt);
    });
  })
    .then((salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) { throw new Error(err); }

        user.password = hash;

        next(null);
      });
    })
    .catch(err => next(err));
});

User.methods.validatePassword = function validatePassword(password) {
  const user = this;

  return new Promise((resolve, reject) => {
    bcrypt.compare(password, user.password, (err, isMatch) => {
      if (err) { return reject('INVALID_PASWORD'); }

      return resolve(isMatch);
    });
  });
};

User.methods.generateToken = function generateToken() {
  const user = this;

  return jwt.sign({ id: user.id }, config.token);
};

export default mongoose.model('user', User);
