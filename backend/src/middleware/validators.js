import { verify } from 'jsonwebtoken';

import config from '../../config';
import { getToken } from '../utils/auth';
import User from '../models/users';

export async function ensureUser(ctx, next) {
  const token = getToken(ctx);

  if (!token) {
    ctx.throw(401, 'NO_TOKEN');
  }

  let decoded = null;
  try {
    decoded = verify(token, config.token);
  } catch (err) {
    ctx.throw(401, 'INVALID_TOKEN');
  }

  ctx.state.userId = decoded.id;
  if (!ctx.state.userId) {
    ctx.throw(401, 'NO_USER');
  }

  return next();
}

export async function ensureAdmin(ctx, next) {
  const user = await User.findById(ctx.state.userId, '-password');
  if (user.type !== 'Admin') {
    ctx.throw(401, 'NOT_ADMIN');
  }

  return next();
}
