export function errorMiddleware() { // eslint-disable-line import/prefer-default-export
  return async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      ctx.status = err.status || 500;
      ctx.body = {
        error: err.message,
      };

      if (err.name === 'ValidationError') {
        ctx.body = {
          errors: Object.keys(err.errors).map(path => err.errors[path]),
        };
      }
      ctx.app.emit('error', err, ctx);
    }
  };
}
