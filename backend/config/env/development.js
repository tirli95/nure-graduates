export default {
  session: 'session-dev',
  token: 'jwt-dev',
  database: process.env.DB || 'mongodb://localhost:27017/nure-graduates-dev',
};
