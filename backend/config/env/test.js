export default {
  session: 'session-test',
  token: 'jwt-test',
  database: 'mongodb://localhost:27017/nure-graduates-test',
};
