export default {
  session: 'session-prod',
  token: 'jwt-prod',
  database: process.env.DB || 'mongodb://localhost:27017/nure-graduates-prod',
};
