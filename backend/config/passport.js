import passport from 'koa-passport';
import { Strategy } from 'passport-local';

import User from '../src/models/users';

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await User.findById(id, '-password');
    done(null, user);
  } catch (err) {
    done(err);
  }
});

passport.use('local', new Strategy({
  usernameField: 'email',
  passwordField: 'password',
}, async (email, password, done) => {
  try {
    const user = await User.findOne({ email });
    if (!user) { return done('INCORRECT_EMAIL'); }

    const isMatch = await user.validatePassword(password);
    if (!isMatch) { return done('INCORRECT_PASSWORD'); }

    return done(null, user);
  } catch (err) {
    return done(err);
  }
}));
