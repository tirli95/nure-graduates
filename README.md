# KNURE assotiation of graduates site

# Зависимости

* node.js v8+
* yarn или npm v5+
* mongoDB

# Для запуска проекта в development режиме

```
cd backend
yarn install или npm install
yarn dev или npm run dev
```

```
cd frontend
yarn install или npm install
APIHOST="http:\\localhost:5000" yarn dev или npm run dev
```

# Для запуска проекта в production режиме

```
cd backend
yarn install или npm install
yarn start или npm start
```

```
cd frontend
yarn install или npm install
APIHOST="АДРЕС BACKEND" yarn start или npm start
```

# Структура проекта


``` bash
.
`-- frontend # клиентская часть проекта
|   `-- pages # основные страницы сайта
|   `-- static # статические файлы
|   |   `-- images # картинки
|   |   `-- locale # файлы с локализацией. Проблемы с переводом править тут
|   |   `-- pages # контент статических страниц. Для изменения содержимого страниц - сюда.
|   `-- hocs # компоненты высшего порядка(high order components)
|   `-- helpers # вспомогательные функции
|   `-- components # React.js компоненты
`-- backend # серверная часть проекта
|   `-- bin # запуск сервера
|   `-- config # конфигурация для различных сред
|   `-- src # исходный код
|   |   `-- middleware
|   |   `-- modules # код обработки роутов и роуты
|   |   `-- models # модели БД
|   |   `-- utils # вспомогательные функции
```